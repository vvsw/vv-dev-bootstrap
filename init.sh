#!/bin/bash

# 3x4=12  4X3=12  9+9+3=21 9+3=12 :) here we are 8+3=11 3+8=11 8-3=5

SSH_KEY_PATH=/home/r0s3/.ssh/e.x1.priv &&\
PYTHON3_PIP_VERSION=18.1-5 &&\
PIP3_VIRTUALENV_VERSION=1.9.1  &&\
VV_DEV_BOOTSTRAP_GIT_REPO=git@gitlab.com:vvsw/vv-dev-bootstrap.git  &&\
VV_DEV_BOOTSTRAP_GIT_BRANCH=dev/0.0.1
VV_DEV_BOOTSTRAP_GIT_LOCAL_PATH=/tmp/tmp_vv_dev_bootstrap
VV_DEV_BOOTSTRAP_PLAYBOOK_PATH=${VV_DEV_BOOTSTRAP_GIT_LOCAL_PATH}/vv-dev-bootstrap-playbook.yml


# install vim editor
sudo apt-get update &&\
sudo apt-get install -y vim  &&\
sudo apt-get remove -y nano &&\

# check if ssh-key exists and load key
if [[ ! -f $SSH_KEY_PATH ]]
then
    echo "[fatal]: ssh key does not exist. please provide correct path to private ssh key!"
    echo "[fatal]: please use command ssh-keygen to generate a new key."
    echo "[fatal]: press any key to exit"
    read press_enter_exit
    exit 1
fi

ssh-add -l $SSH_KEY_PATH
sudo apt-get install -y python3-pip=$PYTHON3_PIP_VERSION python3-dev python3-setuptools rustc python3-wheel python3-venv libssl-dev &&\
pip3 install wheel 



# add virtualenv binary to bash environment path
if [[ ":$PATH:" != *":~/.local/bin':"* ]] 
then
    PATH="~/.local/bin:${PATH}"
fi

python3 -m venv test_venv_here &&\
source test_venv_here/bin/activate  &&\

# install rust installer tool
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
export PATH="$HOME/.cargo/bin:$PATH"
sudo apt-get install rustc

# at this point, virtualenv installed and activated
# install ansible and check version

pip3 install ansible==2.10.6  &&\
ansible --version 
