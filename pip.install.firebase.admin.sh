#!/usr/bin/sh
# work around for installing firebase_admin on debian 2021
pip3 install --upgrade pip
python3 -m pip install --upgrade setuptools
pip3 install --no-cache-dir  --force-reinstall -Iv grpcio
pip3 install --upgrade firebase-admin
