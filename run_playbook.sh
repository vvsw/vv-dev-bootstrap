#!/bin/sh
ANSIBLE_ADDITIONAL_ARGS=$1
VV_ANSIBLE_VAULT_KEY_PATH=.vv_ansible_vault_key

cat << EndOfMessage
 __   __   ______     ______     __     __   __   __     __   __     ______    
/\ \ / /  /\  ___\   /\  == \   /\ \   /\ \ / /  /\ \   /\ "-.\ \   /\  __ \   	
\ \ \'/   \ \  __\   \ \  __<   \ \ \  \ \ \'/   \ \ \  \ \ \-.  \  \ \  __ \  
 \ \__|    \ \_____\  \ \_\ \_\  \ \_\  \ \__|    \ \_\  \ \_\\"\_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/ /_/   \/_/   \/_/      \/_/   \/_/ \/_/   \/_/\/_/ 

(c) 2020 - 2021
author: e
EndOfMessage
                                                             
echo "\n[vv-ansible-info]: Downloading dependencies:"
echo "============================================== \n"
ansible-galaxy install -r vv-dev-bootstrap-ansible-galaxy-requirements.yml --force

echo "\n[vv-ansible-info]: Executing ansible playbook:" 
echo "============================================== \n"
ansible-playbook -e ansible_python_interpreter=/usr/bin/python3 vv-dev-bootstrap-playbook.yml --vault-password-file $VV_ANSIBLE_VAULT_KEY_PATH --ask-become-pass $ANSIBLE_ADDITIONAL_ARGS
 
